const deleteContainer = document.querySelector(".delete");
let projectIdDelete = null;

if(deleteContainer!==null) {
    projectIdDelete = parseInt(deleteContainer.id);
    deleteContainer.addEventListener("click", deletePost);
}

function deletePost() {
    if(projectIdDelete!=null)
        fetch(`/delete/${projectIdDelete}`).then(() => window.location.href = '/');
}